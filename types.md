## Players
[
  <PlayerId>,
  ...
]

## Lives
{
  <PlayerId>: <int Life>,
  ...
}

##  Mana
{
  <PlayerId>: <int Mana>,
  ...
}

## Turns
[
  <PlayerId>: <bool isTurn>,
  ...
]

## Hands
{
  <PlayerId>: {
    <CardId>: <bool isSelected>,
    ...
  },
  ...
}

## Tables
{
  <PlayerId>: [<Minion>, ...],
  ...
}

## MinionInstances
{
  <MinionInstanceId> : {
    <int AttackBuff>,
    <int LifeBuff>,
    <int Damage>,
    <CardDefinitionId>
  }
}

## MinionDefinitions
{
  <MinionDefinitionId>: {
    <int Attack>,
    <int Life>,
    <bool Charge>,
    <bool Taunt>,
    <string Name>
  },
  ...
}

## SpellDefinitions
{
  <SpellDefinitionId>: {
    <int Attack>,
    <int AttackBuff>,
    <int Life>,
    <int LifeBuff>
  },
  ...
}

## CardInstances
{
  <CardInstanceID>: {
    <string ContentType>,
    <SpellDefinitionId>,
    <MinionDefinitionId>
  }
}
