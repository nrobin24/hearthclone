export const CLICK_CARD = 'CLICK_CARD'

export const END_TURN = 'END_TURN'

export const CLICK_TABLE = 'CLICK_TABLE'

export const CLICK_HERO = 'CLICK_HERO'

export const CLICK_MINION = 'CLICK_MINION'

export const START_GAME = 'START_GAME'
