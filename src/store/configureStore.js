import {createStore, applyMiddleware, compose} from 'redux'
import rootReducer from '../reducers'
import createLogger from 'redux-logger'
import Immutable from 'immutable'
import { devTools, persistState } from 'redux-devtools'

export default function configureStore(initialState) {

  const logger = createLogger({
    transformer: (state) => {
      var newState = {};
      for (var i of Object.keys(state)) {
        if (Immutable.Iterable.isIterable(state[i])) {
          newState[i] = state[i].toJS();
        } else {
          newState[i] = state[i];
        }
      };
      return newState;
    }
  });

  const finalCreateStore = compose(
    // Enables your middleware:
    applyMiddleware(logger), // any Redux middleware, e.g. redux-thunk
    // Provides support for DevTools:
    devTools(),
    // Lets you write ?debug_session=<name> in address bar to persist debug sessions
    persistState(window.location.href.match(/[?&]debug_session=([^&]+)\b/))
  )(createStore);

  const store = finalCreateStore(rootReducer);
  // const createStoreWithMiddleware = applyMiddleware(logger)(createStore);
  // const store = createStoreWithMiddleware(rootReducer, initialState);
  // const store = createStore(rootReducer, initialState)
  return store;
}
